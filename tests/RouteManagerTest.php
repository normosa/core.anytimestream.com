<?php
namespace tests;

use Anytimestream\Core\Controllers\Exception\ControllerNotFoundException;
use Anytimestream\Core\Route\RouteLoader;
use Anytimestream\Core\Route\RouteManager;
use PHPUnit\Framework\TestCase;
use tests\Controllers\AboutController;
use tests\Controllers\HomeController;
use tests\Controllers\NotFoundController;
use tests\Request\StaticRequestEngine;

require_once(__DIR__.'/../vendor/autoload.php');

class RouteManagerTest extends TestCase {

    public function testIndexRequest() {
        $routeManager = $this->createRouteManagerFromPath('/home');
        
        $this->assertEquals(false, $routeManager->isIndexRequest());
        
        $routeManager2 = $this->createRouteManagerFromPath('/');
        
        $this->assertEquals(true, $routeManager2->isIndexRequest());
        
        $routeManager3 = $this->createRouteManagerFromPath('');
        
        $this->assertEquals(true, $routeManager3->isIndexRequest());
    }
    
    public function testHomePage() {
        $controller = $this->findControllerFromPath('/home');
        
        $homeController = new HomeController();
        
        $this->assertEquals(true, $controller instanceof $homeController);
        
        try{
            $this->findControllerFromPath('/home/edit');
            $this->assertEquals(true, false);
        } catch (ControllerNotFoundException $ex) {
            $this->assertEquals(true, true);
        }
        
        $controller2 = $this->findControllerFromPath('/');
        
        $this->assertEquals(true, $controller2 instanceof $homeController);
        
        $controller3 = $this->findControllerFromPath('');
        
        $this->assertEquals(true, $controller3 instanceof $homeController);
    }
    
    public function testAboutPage() {
        $controller = $this->findControllerFromPath('/about');
        
        $aboutController = new AboutController();
        
        $this->assertEquals(true, $controller instanceof $aboutController);
        
        $controller2 = $this->findControllerFromPath('/about/');
        
        $this->assertEquals(true, $controller2 instanceof $aboutController);
        
        $controller3 = $this->findControllerFromPath('/about/new');
        
        $this->assertEquals(true, $controller3 instanceof $aboutController);
        
        $controller4 = $this->findControllerFromPath('/about/new/edit');
        
        $this->assertEquals(true, $controller4 instanceof $aboutController);
        
        $controller5 = $this->findControllerFromPath('/about/new/edit/');
        
        $this->assertEquals(true, $controller5 instanceof $aboutController);
    }
    
    public function testNotFound() {
        $controller = $this->findControllerFromPath('/404');
        
        $notFoundController = new NotFoundController();
        
        $this->assertEquals(true, $controller instanceof $notFoundController);
        
        try{
            $this->findControllerFromPath('/fhhfhf');
            $this->assertEquals(true, false);
        } catch (ControllerNotFoundException $ex) {
            $this->assertEquals(true, true);
        }
    }
    
    private function findControllerFromPath($path){
        $routeManager = $this->createRouteManagerFromPath($path);
        return $routeManager->findController();
    }
    
    private function createRouteManagerFromPath($path){
        return new RouteManager($this->getStaticRouteLoader(), new StaticRequestEngine($path));
    }
    
    private function getStaticRouteLoader() {
        return new class implements RouteLoader {
            public function getRoutes(): Array{
                $routes = array();
                $routes['/about/*'] = AboutController::class;
                $routes['/home'] = HomeController::class;
                $routes['/'] = HomeController::class;
                $routes['/404'] = NotFoundController::class;
                return $routes;
            }
        };
    }

}
