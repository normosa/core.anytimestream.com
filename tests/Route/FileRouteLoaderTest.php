<?php
namespace tests\Route;

use Anytimestream\Core\Route\FileRouteLoader;
use PHPUnit\Framework\TestCase;

require_once(__DIR__.'/../../vendor/autoload.php');

class FileRouteLoaderTest extends TestCase{
    
    public function testRoutes(){
        $fileRouteLoader = new FileRouteLoader(__DIR__."/routes.json");
        $this->assertEquals(true, count($fileRouteLoader->getRoutes()) == 5);
        $this->assertEquals(true, isset($fileRouteLoader->getRoutes()['/']));
    }
}