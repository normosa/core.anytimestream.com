<?php
namespace tests\Request;

use PHPUnit\Framework\TestCase;

require_once(__DIR__.'/../../vendor/autoload.php');

class RequestEngineTest extends TestCase{
    
    public function testContextPath(){
        $requestEngine = new StaticRequestEngine("", "google.com", false);
        $this->assertEquals(true, strcmp($requestEngine->getContextPath(), "http://google.com") == 0);
        $this->assertEquals(false, strcmp($requestEngine->getContextPath(), "https://google.com") == 0);
        $this->assertEquals(false, strcmp($requestEngine->getContextPath(), "https://google.com/") == 0);
        
        $requestEngine2 = new StaticRequestEngine("/home", "anytimestream.com", true);
        $this->assertEquals(true, strcmp($requestEngine2->getContextPath(), "https://anytimestream.com") == 0);
        $this->assertEquals(false, strcmp($requestEngine2->getContextPath(), "http://anytimestream.com") == 0);
        $this->assertEquals(false, strcmp($requestEngine2->getContextPath(), "http://anytimestream.com/") == 0);
    }
    
    public function testRoutePaths(){
        $requestEngine = new StaticRequestEngine("");
        $this->assertEquals(true, count($requestEngine->getRoutePaths()) == 0);
        
        $requestEngine2 = new StaticRequestEngine("/");
        $this->assertEquals(true, count($requestEngine2->getRoutePaths()) == 0);
        
        $requestEngine3 = new StaticRequestEngine("/home");
        $this->assertEquals(true, count($requestEngine3->getRoutePaths()) == 1);
        
        $requestEngine4 = new StaticRequestEngine("/home/");
        $this->assertEquals(true, count($requestEngine4->getRoutePaths()) == 1);
        
        $requestEngine5 = new StaticRequestEngine("/home/about/edit/name");
        $this->assertEquals(true, count($requestEngine5->getRoutePaths()) == 4);
        
        $requestEngine6 = new StaticRequestEngine("/home/about/edit/name/");
        $this->assertEquals(true, count($requestEngine6->getRoutePaths()) == 4);
        
        $this->assertEquals(true, strcmp($requestEngine6->getRoutePaths()[0], "home") == 0);
        $this->assertEquals(true, strcmp($requestEngine6->getRoutePaths()[1], "about") == 0);
        $this->assertEquals(true, strcmp($requestEngine6->getRoutePaths()[2], "edit") == 0);
        $this->assertEquals(true, strcmp($requestEngine6->getRoutePaths()[3], "name") == 0);
        
    }
    
}
