<?php
namespace tests\Request;

use Anytimestream\Core\Request\RequestEngine;

class StaticRequestEngine extends RequestEngine {
    
    private $path;
    private $serverName;
    private $isSSL;
    
    public function __construct(string $path, string $serverName = "", bool $isSSL = false) {
        parent::__construct();
        $this->path = $path;
        $this->serverName = $serverName;
        $this->isSSL = $isSSL;
        $this->build();
    }

    public function isSSL(): bool {
        return $this->isSSL;
    }

    public function getURI(): string {
        return $this->path;
    }

    public function getServerName(): string {
        return $this->serverName;
    }

}
