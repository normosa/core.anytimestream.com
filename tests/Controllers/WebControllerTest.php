<?php
namespace tests;

use Anytimestream\Core\Controllers\WebController;
use Anytimestream\UI\HTML;
use PHPUnit\Framework\TestCase;
use tests\Request\StaticRequestEngine;

require_once(__DIR__.'/../../vendor/autoload.php');

class WebControllerTest extends TestCase {

    const TITLE = "WebController";

    public function testAcceptRequest() {
        $webController = $this->createWebController(self::TITLE);
        
        ob_start();
        $webController->acceptRequest(new StaticRequestEngine("/about"));
        $response = ob_get_clean();
        
        $this->assertEquals(false, strcmp($response, self::TITLE) == 0);
    }
    
    private function createWebController(): WebController{
        return new class extends WebController {
            public function index(){
                $this->setHTML(new HTML(WebControllerTest::TITLE));
            }
        };
    }
}
