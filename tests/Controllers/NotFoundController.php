<?php
namespace tests\Controllers;

use Anytimestream\Core\Controllers\Controller;


class NotFoundController extends Controller {
    
    public function index() {
        echo __METHOD__;
    }
}

