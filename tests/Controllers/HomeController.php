<?php
namespace tests\Controllers;

use Anytimestream\Core\Controllers\Controller;

class HomeController extends Controller {
    
    public function index() {
        echo __METHOD__;
    }
}

