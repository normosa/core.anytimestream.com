<?php
namespace tests;

use PHPUnit\Framework\TestCase;
use tests\Controllers\AboutController;
use tests\Request\StaticRequestEngine;

require_once(__DIR__.'/../../vendor/autoload.php');

class ControllerTest extends TestCase {

    public function testAcceptRequest() {
        $aboutController = new AboutController();
        
        $aboutController->acceptRequest(new StaticRequestEngine("/about"));
        
        $this->assertEquals(true, strcmp($aboutController->getCalledMethod(), "index") == 0);
        
        $aboutController->acceptRequest(new StaticRequestEngine("/about/find"));
        
        $this->assertEquals(true, strcmp($aboutController->getCalledMethod(), "find") == 0);
        
        $aboutController->acceptRequest(new StaticRequestEngine("/about/edit"));
        
        $this->assertEquals(true, strcmp($aboutController->getCalledMethod(), "edit") == 0);
        
        $aboutController->acceptRequest(new StaticRequestEngine("/about/delete"));
        
        $this->assertEquals(true, strcmp($aboutController->getCalledMethod(), "delete") == 0);
        
        $aboutController->acceptRequest(new StaticRequestEngine("/about/delete/"));
        
        $this->assertEquals(true, strcmp($aboutController->getCalledMethod(), "delete") == 0);
        
        $aboutController->acceptRequest(new StaticRequestEngine("/about/search/600"));
        
        $this->assertEquals(true, strcmp($aboutController->getCalledMethod(), "search") == 0);
        
        $aboutController->acceptRequest(new StaticRequestEngine("/about/search/600/"));
        
        $this->assertEquals(true, strcmp($aboutController->getCalledMethod(), "search") == 0);
        
        $aboutController->acceptRequest(new StaticRequestEngine("/about/search/600/600"));
        
        $this->assertEquals(true, strcmp($aboutController->getCalledMethod(), "search") != 0);
    }
}
