<?php
namespace tests\Controllers;

use Anytimestream\Core\Controllers\Annotations\Route;
use Anytimestream\Core\Controllers\Controller;

class AboutController extends Controller {
    
    private $calledMethod = "";
    
    public function index() {
        $this->calledMethod = "index";
    }
    
    /**
     * @Route(path="/about/find")
     */
    public function find() {
        $this->calledMethod = "find";
    }
    
    /**
     * @Route(path="/about/edit")
     */
    public function edit() {
        $this->calledMethod = "edit";
    }
    
    /**
     * @Route(path="/about/delete")
     */
    public function delete() {
        $this->calledMethod = "delete";
    }
    
    /**
     * @Route(path="/about/search/#")
     */
    public function search() {
        $this->calledMethod = "search";
    }
    
    public function getCalledMethod(): string{
        return $this->calledMethod;
    }
}

