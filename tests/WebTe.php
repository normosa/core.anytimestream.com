<?php

use PHPUnit\Framework\TestCase;

class WebTest extends TestCase {
    
    const URL = "http://dev.lib.anytimestream.com";

    public function testRouteManager() {
        $reponse = $this->getRequest('');
        $this->assertEquals(true, strpos($reponse, 'Home Page') !== false);
        
        $reponse2 = $this->getRequest('/about');
        $this->assertEquals(true, strpos($reponse2, 'About Page') !== false);
        
        $reponse3 = $this->getRequest('/home');
        $this->assertEquals(true, strpos($reponse3, 'Home Page') !== false);
        
        $reponse4 = $this->getRequest('/not-found');
        $this->assertEquals(true, strpos($reponse4, 'URL not found') !== false);
        
        $reponse5 = $this->getRequest('/about');
        $this->assertEquals(false, strpos($reponse5, 'Home Page') !== false);
    }

    private function getRequest($path) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::URL.$path);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        $response = curl_exec($ch);

        curl_close($ch);
        
        return $response;
    }

}
