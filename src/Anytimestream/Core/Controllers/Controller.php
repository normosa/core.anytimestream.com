<?php

/* ~ Controller.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - Core                      |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\Core\Controllers;

use Anytimestream\Core\Controllers\Annotations\Route;
use Anytimestream\Core\Request\RequestEngine;
use Anytimestream\Core\Request\URI;
use Anytimestream\Core\Request\URIMatcher;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use ReflectionClass;
use ReflectionMethod;

/**
 * Anytimestream Core
 * abstract class for Controller
 * @author Norman Osaruyi
 * @package Anytimestream\Core\Controllers
 */
abstract class Controller {

    /**
     * Creates instance
     */
    function __construct() {
        AnnotationRegistry::registerFile($this->getBaseDir().Route::class.'.php');
    }
    
    /**
     * Gets base directory
     * @return string dir
     */
    private function getBaseDir(): string {
        return substr(__DIR__, 0, strlen(__DIR__) - strlen(__NAMESPACE__));
    }

    /**
     * Accepts Request
     * @param RequestEngine $requestEngine request engine
     */
    public function acceptRequest(RequestEngine $requestEngine) {
       $routes = $this->getRoutes();
       krsort($routes);
       $method = $this->findRouteMethod($routes, $requestEngine->getURI());
       call_user_func_array(array($this, $method), array());
    }

    /**
     * Gets routes
     * @return Array routes
     */
    private function getRoutes(): Array {
        $methods = (new ReflectionClass($this))->getMethods();
        $routes = array();
        foreach ($methods as $method){
            $reflectionMethod = new ReflectionMethod(get_class($this), $method->name);
            $routeAnnotation = (new AnnotationReader())->getMethodAnnotation($reflectionMethod, Route::class);
            if($routeAnnotation != null){
                $routes[$routeAnnotation->path] = $method->name;
            }
        }
        return $routes;
    }
    
    /**
     * Find route handler
     * @return string route handler
     */
    private function findRouteMethod(Array $routes, string $uri): string{
        $uriMatacher = new URIMatcher(new URI($uri));
        foreach ($routes as $path => $method) {
           if($uriMatacher->containsPath($path)){
               return $method;
           }
       }
       return "index";
    }
    
    /**
     * Default route handler
     */
    abstract function index();
}
