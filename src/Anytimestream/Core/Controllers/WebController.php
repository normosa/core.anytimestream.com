<?php
/* ~ WebController.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - Core                      |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */
namespace Anytimestream\Core\Controllers;

use Anytimestream\Core\Request\RequestEngine;
use Anytimestream\UI\HTML;
/**
 * Anytimestream Core
 * abstract class for WebController
 * @author Norman Osaruyi
 * @package Anytimestream\Core\Controllers
 */
abstract class WebController extends Controller{
    
    private $html;
    
    /**
     * Gets HTML
     * @return HTML html
     */
    public function getHTML(): HTML{
        return $this->html;
    }
    
    /**
     * Sets HTML
     * @param HTML $html html
     */
    public function setHTML(HTML $html){
        $this->html = $html;
    }
    
    /**
     * Accepts Request
     * @param RequestEngine $requestEngine request engine
     */
    public function acceptRequest(RequestEngine $requestEngine) {
        parent::acceptRequest($requestEngine);
        $this->renderView();
    }

    /**
     * Render HTML
     */
    private function renderView(){
        $this->getHTML()->render();
    }
}