<?php
/* ~ NotImplementedException.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - Core Library              |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\Core\Exception;

use Exception;

/**
 * Anytimestream Core
 * class for NotImplementedException
 * @author Norman Osaruyi
 * @package Anytimestream\Core\Exception
 */
class NotImplementedException extends Exception {
    
}

