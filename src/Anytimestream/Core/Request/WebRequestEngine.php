<?php

/* ~ WebRequestEngine.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - Core Library              |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\Core\Request;

/**
 * Anytimestream Core
 * class for Managing Requests
 * @author Norman Osaruyi
 * @package Anytimestream\Core\Request
 */
class WebRequestEngine extends RequestEngine{

    private static $instance;

    /**
     * Gets Singleton instance
     */
    public static function GetInstance() {
        if (self::$instance == null) {
            self::$instance = new WebRequestEngine();
            self::$instance->build();
        }
        return self::$instance;
    }
    
    /**
     * Gets URI
     * @return string uri
     */
    public function getURI(): string{
        return strtok(filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL), '?');
    }
    
    /**
     * Gets QueryString
     * @return string querystring
     */
    public function getQueryString(): string {
        return filter_input(INPUT_SERVER, 'QUERY_STRING', FILTER_SANITIZE_URL);
    }
    
    /**
     * Gets Remote Address
     * @return string remote address
     */
    public function getRemoteAddress(): string {
        return (filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR', FILTER_SANITIZE_STRING) != null)? filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR', FILTER_SANITIZE_STRING): filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_SANITIZE_STRING);
    }
    
    /**
     * Gets User Agent
     * @return string useragent
     */
    public function getUserAgent(): string {
        return (filter_input(INPUT_SERVER, 'HTTP_USER_AGENT', FILTER_SANITIZE_STRING) != null)? filter_input(INPUT_SERVER, 'HTTP_USER_AGENT', FILTER_SANITIZE_STRING): '';
    }
    
    /**
     * Checks if request is SSL
     * @return bool true or false
     */
    public function isSSL(): bool{
        $serverHttps = filter_input(INPUT_SERVER, 'HTTPS', FILTER_SANITIZE_STRING);
        return (!empty($serverHttps) && $serverHttps != 'off');
    }
    
    /**
     * Gets server name
     * @return srring server name
     */
    public function getServerName(): string {
        return filter_input(INPUT_SERVER, 'SERVER_NAME', FILTER_SANITIZE_URL);
    }

}
