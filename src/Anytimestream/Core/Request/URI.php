<?php
/* ~ URI.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - Core Library              |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\Core\Request;

/**
 * Anytimestream Core
 * class for URI
 * @author Norman Osaruyi
 * @package Anytimestream\Core\Request
 */
class URI {
    
    private $path;
    
    /**
     * Creates instance
     * @param string $path uri path
     */
    public function __construct(string $path = "") {
        $this->path = $path;
    }
    
    /**
     * Gets path
     * @return string uri path
     */
    public function getPath(): string {
        return $this->path;
    }
    
    /**
     * Sets path
     * @param string $path path
     */
    public function setPath(string $path){
        $this->path = $path;
    }
}
