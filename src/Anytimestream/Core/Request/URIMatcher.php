<?php

/* ~ URIMatcher.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - Core Library              |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\Core\Request;

/**
 * Anytimestream Core
 * class for URI matching
 * @author Norman Osaruyi
 * @package Anytimestream\Core\Request
 */
class URIMatcher {

    private $uri;

    /**
     * Creates instance
     * @param URI $uri url
     */
    public function __construct(URI $uri = null) {
        $this->uri = $uri;
    }

    /**
     * Checks if a path is part of uri
     * @param string $path path to match
     * @return bool true or false
     */
    public function containsPath(string $path): bool {
        if ($this->pathEndsWithExtras($path)) {
            return $this->matchesPathWithExtras($path);
        } else {
            return $this->matchesPath($path);
        }
    }

    /**
     * Checks if path ends with extras
     * @param string $path path to match
     * @return bool true or false
     */
    public function pathEndsWithExtras(string $path): bool {
        $pathLen = strlen($path);
        return ($pathLen > 2 && (strcmp(substr($path, $pathLen - 2), "/*") == 0 || strcmp(substr($path, $pathLen - 2), "/#") == 0));
    }

    /**
     * Checks if a path is part of uri extras
     * @param string $path path to match
     * @return bool true or false
     */
    public function matchesPathWithExtras(string $path): bool {
        if ($this->isWildcardPath($path)) {
            return $this->matchesPathWithWildcard($path);
        } else if ($this->isIDPath($path)) {
            return $this->matchesPathWithId($path);
        }
        return false;
    }

    /**
     * Checks if a path matches uri
     * @param string $path path to match
     * @return bool true or false
     */
    public function matchesPath($path): bool {
        return ((strcmp($this->uri->getPath(), $path) == 0) || (strcmp($this->uri->getPath(), $path . '/') == 0));
    }

    /**
     * Checks if a path is wild card
     * @param string $path path to match
     * @return bool true or false
     */
    public function isWildcardPath(string $path): bool {
        $pathLen = strlen($path);
        return (strcmp(substr($path, $pathLen - 2), "/*") == 0);
    }

    /**
     * Checks if a path ends with id
     * @param string $path path to match
     * @return bool true or false
     */
    public function isIDPath(string $path): bool {
        $pathLen = strlen($path);
        return (strcmp(substr($path, $pathLen - 2), "/#") == 0);
    }

    /**
     * Checks if a path is part of wildcard
     * @param string $path path to match
     * @return bool true or false
     */
    public function matchesPathWithWildcard(string $path): bool {
        $pathLen = strlen($path);
        return ((strcmp(substr($this->uri->getPath(), 0, $pathLen - 2), substr($path, 0, $pathLen - 2)) == 0));
    }

    /**
     * Checks if a path matches uri with id
     * @param string $path path to match
     * @return bool true or false
     */
    public function matchesPathWithId(string $path): bool {
        $uriPaths = array_filter(explode('/', $this->uri->getPath()));
        $uriPathLen = count($uriPaths);
        $paths = array_filter(explode('/', $path));
        $pathLen = count($paths);
        array_pop($uriPaths);
        array_pop($paths);
        return ($uriPathLen == $pathLen && (strcmp(implode("/", $uriPaths), implode("/", $paths)) == 0));
    }

}
