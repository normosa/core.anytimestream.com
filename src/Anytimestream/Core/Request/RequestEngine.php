<?php

/* ~ RequestEngine.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - Core                      |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\Core\Request;

use Anytimestream\Core\Exception\NotImplementedException;

/**
 * Anytimestream Core
 * interface for RequestEngine
 * @author Norman Osaruyi
 * @package Anytimestream\Core\Request
 */
abstract class RequestEngine {

    private $routePaths;
    private $uri;

    /**
     * default constructor
     */
    protected function __construct() {
        
    }

    /**
     * build engine
     */
    protected function build() {
        $this->uri = $this->getURI();
        $this->setRoutePaths();
    }

    /**
     * Gets URI
     * @return string uri
     */
    public function getURI(): string {
        throw new NotImplementedException();
    }
    
    /**
     * Gets Remote Address
     * @return string remote address
     */
    public function getRemoteAddress(): string{
        throw new NotImplementedException();
    }
    
    /**
     * Gets User Agent
     * @return string useragent
     */
    public function getUserAgent(): string{
        throw new NotImplementedException();
    }
    
    /**
     * Gets QueryString
     * @return string querystring
     */
    public function getQueryString(): string{
        throw new NotImplementedException();
    }
    /**
     * Sets route paths
     */
    private function setRoutePaths() {
        $this->routePaths = $this->filterPaths(explode('/', $this->uri));
    }

    /**
     * filters paths
     * @param Array $array paths
     * @return Array filtered paths 
     */
    private function filterPaths($array): Array {
        return array_values(array_filter($array));
    }

    /**
     * Gets route paths
     * @return Array route paths
     */
    public function getRoutePaths(): Array {
        return $this->routePaths;
    }

    /**
     * gets context path
     * @return string context path
     */
    public function getContextPath(): string {
        $protocol = ($this->isSSL()) ? 'https://' : 'http://';
        return $protocol . $this->getServerName();
    }

    /**
     * Checks if request is SSL
     * @return bool true or false
     */
    abstract public function isSSL(): bool;

    /**
     * Gets server name
     * @return srring server name
     */
    abstract public function getServerName(): string;
}
