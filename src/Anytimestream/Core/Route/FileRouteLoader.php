<?php
/* ~ FileRouteLoader.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - Core                      |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\Core\Route;

/**
 * Anytimestream Core
 * class for Loading Routes
 * @author Norman Osaruyi
 * @package Anytimestream\Core\Route
 */
class FileRouteLoader implements RouteLoader{
    
    private $filename;
    
    /**
     * Creates instance
     * @param string $filename filename containing routes
     */
    public function __construct(string $filename) {
        $this->filename = $filename;
    }
    
    /**
     * Gets routes
     * @return Array Routes
     */
    public function getRoutes(): Array {
        return json_decode(file_get_contents($this->filename), true);
    }
}
