<?php

/* ~ RouteManager.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - Core Library              |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\Core\Route;

use Anytimestream\Core\Controllers\Controller;
use Anytimestream\Core\Controllers\Exception\ControllerNotFoundException;
use Anytimestream\Core\Request\RequestEngine;
use Anytimestream\Core\Request\URI;
use Anytimestream\Core\Request\URIMatcher;

/**
 * Anytimestream Core
 * class for Managing Routes
 * @author Norman Osaruyi
 * @package Anytimestream\Core\Route
 */
class RouteManager {

    private static $instance;
    private $routes;
    private $requestEngine;

    /**
     * Creates new Instance
     * @param RouteLoader $routeLoader loads routes
     * @param RequestEngine $requestEngine request engine
     */
    public function __construct(RouteLoader $routeLoader, RequestEngine $requestEngine) {
        $this->initialize($routeLoader, $requestEngine);
    }

    /**
     * Initialize RouteManager
     * @param RouteLoader $routeLoader loads routes
     * @param RequestEngine $requestEngine request engine
     */
    private function initialize(RouteLoader $routeLoader, RequestEngine $requestEngine) {
        $this->routes = $routeLoader->getRoutes();
        krsort($this->routes);
        $this->requestEngine = $requestEngine;
    }

    /**
     * Initialize Singleton RouteManager
     * @param RouteLoader $routeLoader loads routes
     * @param RequestEngine $requestEngine request engine
     * @return RouteManager singleton route manager
     */
    public static function GetInstance(RouteLoader $routeLoader, RequestEngine $requestEngine): RouteManager {
        if (self::$instance == null) {
            self::$instance = new RouteManager($routeLoader, $requestEngine);
        }
        return self::$instance;
    }

    /**
     * Route request to controllers
     */
    public function routeRequest() {
        try {
            $controller = $this->findController();
            $controller->acceptRequest($this->requestEngine);
        } catch (ControllerNotFoundException $ex) {
            $this->redirectWith404();
        }
    }

    
    /**
     * Find Controller
     * @return Controller controller found
     */
    public function findController(): Controller {
        if ($this->isIndexRequest()) {
            return $this->getIndexController();
        }
        $uriMatcher = new URIMatcher(new URI($this->requestEngine->getURI()));
        foreach ($this->routes as $routePath => $strController) {
            if ($uriMatcher->containsPath($routePath)) {
                return (new $strController);
            }
        }
        throw new ControllerNotFoundException();
    }

    /**
     * Checks for default route request
     * @return bool true or false
     */
    public function isIndexRequest(): bool {
        return (count($this->requestEngine->getRoutePaths()) == 0) ? true : false;
    }

    /**
     * Gets default route controller
     * @return IController Controller
     */
    private function getIndexController(): Controller {
        return (new $this->routes['/']);
    }
    
    /**
     * Redirects request
     */
    private function redirectWith404() {
        header('location: ' . $this->requestEngine->getContextPath() . '/404?err_url=' . $this->requestEngine->getURI());
        exit;
    }

}
