<?php
/* ~ RouteLoader.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - Core                      |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\Core\Route;

/**
 * Anytimestream Core
 * interface for Loading Routes
 * @author Norman Osaruyi
 * @package Anytimestream\Core\Route
 */
interface RouteLoader {
    
    /**
     * Gets routes
     * @return Array Routes
     */
    public function getRoutes(): Array;
}
