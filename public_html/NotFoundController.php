<?php
namespace tests;

use Anytimestream\Lib\Controllers\Controller;
use Anytimestream\UI\Page;
use Anytimestream\UI\RawContent;

class NotFoundController extends Controller {
    
    public function acceptRequest() {
        $page = new Page();
        $page->setTitle("Not found Page");
        $page->setContent(new RawContent("<h2>URL not found</h2>"));
        $this->setPage($page);
    }
}

