<?php
namespace public_html;

use Anytimestream\Core\Request\WebRequestEngine;
use Anytimestream\Core\Route\RouteLoader;
use Anytimestream\Core\Route\RouteManager;

require_once(__DIR__.'/../vendor/autoload.php');

RouteManager::GetInstance(new class implements RouteLoader{
    
    public function getRoutes(): Array {
        return json_decode(file_get_contents('routes.json'), true);
    }
}, WebRequestEngine::GetInstance())->routeRequest();
