<?php
namespace public_html;

use Anytimestream\Core\Controllers\Annotations\Route;
use Anytimestream\Core\Controllers\WebController;
use Anytimestream\UI\HTML;


class HomeController extends WebController {
    
    /**
     * @Route(path="/")
     */
    public function index() {
        $this->setHTML(new HTML("index"));
    }
    
    /**
     * @Route(path="/home")
     */
    public function home() {
        $this->setHTML(new HTML("home"));
    }
    
    /**
     * @Route(path="/home/edit")
     */
    public function edit() {
        $this->setHTML(new HTML("edit"));
    }
    
    /**
     * @Route(path="/home/edit/user/#")
     */
    public function user() {
        $this->setHTML(new HTML("user"));
    }
}

