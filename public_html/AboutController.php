<?php
namespace tests;

use Anytimestream\Lib\Controllers\Controller;
use Anytimestream\UI\Page;
use Anytimestream\UI\RawContent;

class AboutController extends Controller {
    
    public function acceptRequest() {
        $page = new Page();
        $page->setTitle("About Page");
        $page->setContent(new RawContent("<h2>About Page</h2>"));
        $this->setPage($page);
    }
}

